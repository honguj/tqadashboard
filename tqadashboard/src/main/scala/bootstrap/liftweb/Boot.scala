package bootstrap.liftweb

import net.liftweb._
import sitemap.Loc.If
import util._

import http._
import sitemap._
import vn.timquanan.dashboard.rest.lib.{UserService, QuananService, RestMongo}
import vn.timquanan.dashboard.snippet.{LogInForm, RestaurantInfo}

class Boot {
  def boot {

    // DB Login
    RestMongo.start

    // Add all packages
    LiftRules.addToPackages( "vn.timquanan.dashboard" )

    // Web
		val entries = List( 
			Menu.i("Home") / "index",
      Menu.i("Restaurants")/ "Restaurants" >> If(() => LogInForm.isLoggedIn, () => RedirectResponse("/index")) ,
      Menu( RestaurantInfo.loc ) ,
			Menu.i("About") / "extra" / "about"
		)

		LiftRules.setSiteMap(SiteMap(entries:_*))

		LiftRules.early.append(_.setCharacterEncoding("UTF-8"))

		LiftRules.htmlProperties.default.set( (r: Req) =>
			new Html5Properties(r.userAgent)
		)


    // REST dispatch
    LiftRules.dispatch.append( QuananService )
    LiftRules.dispatch.append( UserService )

	}
}
