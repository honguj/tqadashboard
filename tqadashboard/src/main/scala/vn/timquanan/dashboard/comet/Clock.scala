package vn.timquanan.dashboard.comet

import net.liftweb.http.CometActor
import net.liftweb.common.Full
import java.util.Date
import net.liftweb.http.js.JsCmds.SetHtml
import xml.Text
import net.liftweb.util._
import net.liftweb.util.Schedule._

import net.liftweb.util.Helpers._

case object Tick

class Clock extends CometActor {
  override def defaultPrefix = Full("clk")

  def theTime = TimeHelpers.timeNow.toString
  def render = "#time *" #> theTime

  // schedule a ping every 10 seconds so we redraw
  schedule(this, Tick, 2000L)

  override def lowPriority : PartialFunction[Any, Unit] = {
    case Tick => {
      println("Got tick " + new Date());
      partialUpdate(SetHtml("time", Text(theTime) ))

      // schedule an update in 10 seconds
      schedule(this, Tick, 2000L)
    }
  }
}

