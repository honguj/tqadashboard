package vn.timquanan.dashboard.comet

import net.liftweb.actor.LiftActor
import net.liftweb.http.ListenerManager
import vn.timquanan.dashboard.rest.model.Quanan

object RestaurantsManager extends LiftActor with ListenerManager
{
  private var msgs = new NewRestaurant( new Quanan)
  def createUpdate = msgs

  override def lowPriority = {
     case nr: NewRestaurant => {
       msgs = nr
       updateListeners()
     }
  }
}
