package vn.timquanan.dashboard.comet

import net.liftweb.http.{CometListener, CometActor}
import net.liftweb.http.js.JsCmds.SetHtml


import xml.Text
import net.liftweb.util.TimeHelpers
import net.liftweb.http.js.JsCmds
import net.liftweb.http.js.jquery.JqJsCmds._
import vn.timquanan.dashboard.rest.model.Quanan

case class NewRestaurant(quanan: Quanan)


class RestaurantComet extends CometActor with CometListener{

  def render = "#newData *" #> (<span></span>)

  def registerWith = RestaurantsManager

  def newMessage(quanan: Quanan) =
    (
    <div class="date-restaurants">
    <div class="date">{quanan.created_at.toString()}</div>
    <ul class="restaurants">
      <li class="restaurant">
        <a class="title" href={quanan.source_s.toString()}> {quanan.title_s} </a>
      </li>
    </ul></div>)

  override def lowPriority : PartialFunction[Any, Unit] = {
    case NewRestaurant(quanan) => {
      println( "new resto =" + quanan.title_s + " - " + quanan.source_s )
      // Desactivate temporary comet actor
      //partialUpdate(AppendHtml("newData", newMessage(quanan)))
    }
  }

}
