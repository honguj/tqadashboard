package vn.timquanan.dashboard
package controler

import rest.model.Quanan
import scala.util.matching._

object QuananDB
{
  private val months = Map( "Jan" -> "01" , "Feb" -> "02", "Mar" -> "03", "Apr" -> "04", "May" -> "05", "Jun" -> "06",
                  "Jul" -> "07", "Aug" -> "08", "Sep" -> "09", "Oct" -> "10", "Nov" -> "11", "Dec" -> "12")
  private val dateRegex = new Regex("""([a-zA-Z]*) ([a-zA-Z]*) ([0-9 ]*) (\d\d:\d\d:\d\d) ([A-Z]*) (\d\d\d\d)""",
    "dayS", "month", "day" , "hours", "timezone", "years")

  def getAllRestaurants() = Quanan.findAllQuanan()

  def getAllRestaurantsByCreatedDate() = //: List[(String, Map[(String, List[Quanan])])]
  {
    val r = getAllRestaurants().toList.groupBy( (e: Quanan)=> dateParser(e.created_at.toString())._1 ).toList
    val result = for { e <- r }
      yield ( e._1, e._2.groupBy( (q: Quanan) => sourceParser(q.source_s.toString()) ) )
    result.filterNot(result => result._1 == "Invalid date").toList.sortWith( _._1.toString() > _._1.toString() )
  }

  def dateParser(datetime: String): (String, String) =
  {
    datetime match {
      case dateRegex(dayS, month, day, hours, timezone, year) => ((year + " / " + convertMonth(month) + " / " + day ).toString, hours)
      case _ => ("Invalid date", "Invalid hour")
    }
  }

  def sourceParser(source: String): String =
  {
    val sourceRegex = new Regex("""([http://]{0,7})([w]{0,3}[.]{0,1})([^.]*).([a-z]*)/([^@]*)""","h", "w", "source","domain", "others")
    source match {
      case sourceRegex(h, w, source, domain, others) => source+"."+domain
      case _ => "Link doesnt exist"
    }
  }

  def convertMonth(monthS: String) = months(monthS)

}
