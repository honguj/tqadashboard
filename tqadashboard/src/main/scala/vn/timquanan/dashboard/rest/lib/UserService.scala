package vn.timquanan.dashboard.rest.lib

import net.liftweb.http.rest.RestHelper
import vn.timquanan.dashboard.rest.model.{Quanan, User}
import net.liftweb.http.OkResponse

object UserService extends RestHelper
{
  serve
  {
    // Add a new User
    case "user" :: Nil XmlPut User(user) -> _ =>
    {
      user.save
      OkResponse()
    }
  }

}