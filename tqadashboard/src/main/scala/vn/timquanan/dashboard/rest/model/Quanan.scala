package vn.timquanan.dashboard.rest.model

import net.liftweb._
import common.Full

import json.JsonAST.{JString, JValue}
import json.{Extraction, Xml}

import mongodb.record.{MongoMetaRecord, MongoId, MongoRecord}
import record.field._
import vn.timquanan.dashboard.rest.lib.RestMongo
import xml.Node

import org.bson.types.ObjectId

import com.foursquare.rogue.Rogue._

import org.joda.time.DateTime

class Quanan extends MongoRecord[Quanan] with MongoId[Quanan]
{
  def meta = Quanan

  object source_s extends StringField( this, 1000 )
  object title_s extends StringField( this, 1000 )
  object qa_dia_chi extends StringField(this, 2000)
  object created_at extends StringField(this, 100)
}

object Quanan extends Quanan with MongoMetaRecord[Quanan]
{
  // Find all Quanan in db
  def findAllQuanan():List[Quanan] = Quanan.findAll

  // Find a specific restaurant by Id
  def getById(id: String) = findAllQuanan().find( _._id.toString == id)

  // Find id of a specific restaurant
  def fromId( quan: Quanan ) = quan._id.toString()

  // Find a specific restaurant by name
  def getByName(name: String) = findAllQuanan().find( _.title_s.toString() == name)

  def updateDateTime(id: String, date: DateTime) =
  {
    ( Quanan.where(_._id eqs new ObjectId( id ) )
      modify(_.created_at setTo date.toString())
      updateOne()
     )
  }

  override def mongoIdentifier = RestMongo

  implicit def toXml(quanan : Quanan): Node = <quanan>{Xml.toXml(quanan)}</quanan>
  implicit def toJson(quanan : Quanan): JValue = Extraction.decompose(quanan)

  /* Extract JSON value to a Quanan object */
  def unapply(in: JValue): Option[Quanan] = apply(in)

  def unapply(in: Node): Option[Quanan] = apply( Xml.toJson( in ) )

  /* Extract JSON value to a Quanan object */
  def apply(in: JValue): Option[Quanan] =
  {
    val clientRequest = in.values.asInstanceOf[Map[String,String]]
    val g = Quanan.createRecord

    g.setFieldsFromJValue( in ) match {
      case Full( _ ) => {
        Some( g )
      }
      case _ => None
    }
  }
}