package vn.timquanan.dashboard.rest.lib

import net.liftweb.http.rest.RestHelper
import vn.timquanan.dashboard.rest.model.Quanan
import net.liftweb.http.{OkResponse}
import net.liftweb.json.JsonAST.{JString}

import vn.timquanan.dashboard.comet.{RestaurantsManager, NewRestaurant}

import org.joda.time.{DateTimeZone, DateTime}

object QuananService extends RestHelper
{
  serve{
    // Get all quanan
    case "Quanan" :: Nil XmlGet _ => {
      <tqa>{Quanan.findAllQuanan() flatMap {q =>
        <quanan id={q.id.toString} title={q.title_s.toString()} link={q.source_s.toString() } created={q.created_at.toString()}/>
      }}</tqa>
    }

    case "Quanan" :: Nil JsonGet _ => {
      JString(Quanan.findAllQuanan().toString())
    }

    // Get a specific quanan
    case "Quanan" :: id :: Nil JsonGet _ => {
      val t = for {
        q <- Quanan.find( id ) ?~ "Map doesn't exist" ~> 404
      } yield {q}
      JString(t.toString())
    }

    // Add a new Quanan
    case "Quanan" :: Nil XmlPut Quanan(quanan) -> _ =>
    {
      val unixTime: Long = System.currentTimeMillis / 1000L
      val isoDate = new DateTime(unixTime*1000, DateTimeZone.forID("Asia/Ho_Chi_Minh")).toDateTimeISO
      println("Date = " + isoDate)
      quanan.save
      Quanan.updateDateTime(quanan._id.toString(), isoDate)

      // Temporary remove Comet architecture
      //RestaurantsManager ! NewRestaurant( quanan )
      OkResponse()
    }

  }

}
