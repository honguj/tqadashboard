package vn.timquanan.dashboard.rest.lib

import net.liftweb.mongodb.{MongoDB, MongoIdentifier, MongoAddress, MongoHost}
import com.mongodb.{Mongo, MongoOptions, ServerAddress}
import net.liftweb.util.Props

object RestMongo extends MongoIdentifier
{
        override def jndiName = "rest_mongo"

        private var mongo: Option[Mongo] = None

        def start = {

                val address = Props.get( "mongo.address" ).openOr("thanhdeptrai")
                val port = Props.get( "mongo.port" ).openOr("27017").toInt
                val database = Props.get( "mongo.database" ).openOr("web_quanan_test")

                mongo = Some( new Mongo( new ServerAddress( address, port ) ) )

                Props.mode match {
                        case Props.RunModes.Production => {
                          val username = Props.get( "mongo.username" ).openOr("")
                          val password = Props.get( "mongo.password" ).openOr("")

                          MongoDB.defineDbAuth(
                                   RestMongo,
                                   mongo.get,
                                   database,
                                   username,
                                   password
                          )
                        }
                        case _ => {
                          MongoDB.defineDb(
                                    RestMongo,
                                    mongo.get,
                                    database
                                 )}
                }
        }

        def stop = {
          mongo.foreach(_.close)
          MongoDB.close
          mongo = None
        }
}