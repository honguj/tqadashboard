package vn.timquanan.dashboard.rest.model

import net.liftweb._
import common.{Empty, Full}
import json.JsonAST.JValue
import json.{Extraction, Xml}
import mongodb.record.{MongoMetaRecord, MongoId, MongoRecord}

import record.field._

import vn.timquanan.dashboard.rest.lib.RestMongo

import xml.Node

import com.foursquare.rogue.Rogue._

class User extends MongoRecord[User] with MongoId[User]{
  def meta = User

  object userName extends StringField( this, 1000 )
  object password extends StringField( this, 1000 )
}

object User extends User with MongoMetaRecord[User]{

  override def mongoIdentifier = RestMongo

  def authUser( username: String, password: String ): Boolean = {
    ( User where( _.userName eqs username )
      where( _.password eqs password )
      fetch( 1 ) ) match
    {
      case head :: Nil => {
        Full( head )
        true
      }
      case _ => {
        Empty
        false
      }
    }
  }

  implicit def toXml(user : User): Node = <user>{Xml.toXml(user)}</user>
  implicit def toJson(user : User): JValue = Extraction.decompose(user)

  def unapply(in: JValue): Option[User] = apply(in)

  def unapply(in: Node): Option[User] = apply( Xml.toJson( in ) )

  /* Extract JSON value to a User object */
  def apply(in: JValue): Option[User] =
  {
    val g = User.createRecord
    g.setFieldsFromJValue( in ) match {
      case Full( _ ) => {
        Some( g )
      }
      case _ => None
    }
  }
}
