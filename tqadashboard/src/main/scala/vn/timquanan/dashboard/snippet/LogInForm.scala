package vn.timquanan.dashboard.snippet

import scala.xml.{NodeSeq}
import net.liftweb.http._
import net.liftweb.util.Helpers._
import vn.timquanan.dashboard.rest.model.User


object LogInForm {
  private object name extends SessionVar("")
  private object password extends SessionVar("")
  private object referer extends RequestVar(S.referer openOr "/Restaurants")
  var isLoggedIn = false

  def loggedIn(html: NodeSeq) =
    if (isLoggedIn) html else NodeSeq.Empty

  def loggedOut(html: NodeSeq) =
    if (!isLoggedIn) html else NodeSeq.Empty

  def logIn = {
    def processLogIn() {
      isValidName(name) match { //Validator.
        case true => {
          User.authUser(name, password) match { //Validator.
            case true => {
              isLoggedIn = true
              S.redirectTo("/Restaurants")
            } // Success: logged in
            case _ => S.error("password", "Invalid username/password!")
          }
        }
        case _ => S.error("name", "Invalid username format!")
      }
    }

    val r = referer.is
    "name=name" #> SHtml.textElem(name) &
      "name=password" #> (
        SHtml.passwordElem(password) ++
          SHtml.hidden(() => referer.set(r))) &
      "type=submit" #> SHtml.onSubmitUnit(processLogIn)
  }

  def logOut = {
    def processLogOut() {
      isLoggedIn = false
      S.redirectTo("/index")
    }
    "#btnLogOut" #> SHtml.onSubmitUnit(processLogOut)
  }

  def getName = "*" #> name.is
  def getPw = "*" #> password.is

  def isValidName(name: String) = true
  def isValidLogin(name: String,  password: String) = true
}