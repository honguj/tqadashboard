package vn.timquanan.dashboard
package snippet

import net.liftweb.http._
import net.liftweb.util.Helpers._

import vn.timquanan.dashboard.rest.model._
import xml.NodeSeq
import net.liftweb.sitemap.{*, Menu}
import controler.QuananDB
import net.liftweb.common.Full
import net.liftweb.sitemap.Loc.{If, EarlyResponse}
import net.liftweb.sitemap.Loc.If

object RestaurantInfo
{
  // permet de convertir un url dans un cheminement
  // url: cheminement / hash
  // https://www.assembla.com/spaces/liftweb/wiki/Location_Parameters
  val menu = Menu.param[Quanan](
    "RestaurantInfo",
    "RestaurantInfo",
    //id => asInt( id ).map( i => Quanan.get(i) ),
    Quanan.getById _,
    Quanan.fromId _
  ) / "RestaurantInfo" / * >> If(() => LogInForm.isLoggedIn, () => RedirectResponse("/index"))

  lazy val loc = menu.toLoc
}


class RestaurantInfo ( restaurant: Quanan ) extends DispatchSnippet
{
  // pour eviter de faire de la reflexion dans le html
  // ( https://www.assembla.com/spaces/liftweb/wiki/More_on_Snippets )
  def dispatch = { case "render" => render }

  def render: NodeSeq => NodeSeq  =
  {
    ".date *" #> restaurant.created_at &
    ".title *" #> restaurant.title_s &
    ".source *" #> restaurant.source_s &
      ".source [href]" #> restaurant.source_s &
    ".address *" #> restaurant.qa_dia_chi
  }
}
