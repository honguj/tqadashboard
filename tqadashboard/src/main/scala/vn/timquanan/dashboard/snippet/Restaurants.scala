package vn.timquanan.dashboard.snippet

import net.liftweb.util.Helpers._
import net.liftweb.http.DispatchSnippet
import vn.timquanan.dashboard._
import controler.QuananDB
import rest.model.Quanan

object Restaurants extends DispatchSnippet{
  override def dispatch =
  {
    case "render" => render
  }

  def render =
  {
    ".date-restaurants *" #> QuananDB.getAllRestaurantsByCreatedDate().map { dateRestos =>
        ".date *" #> dateRestos._1 &
        ".domainList *" #> dateRestos._2.toList.map { sourceRestos: (String, List[Quanan]) =>
          ".domainName *" #> (sourceRestos._1 + "  (" + sourceRestos._2.size + " news )") &
            ".restaurant *" #> sourceRestos._2.map { resto: Quanan =>
              ".title *" #> resto.title_s &
              ".title [href]" #> RestaurantInfo.menu.calcHref( resto )
            }
        }
    }
  }
}