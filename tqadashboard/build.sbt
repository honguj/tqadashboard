name := "tqaDashBoard"

version := "0.0.1"

organization := "tqaDashBoard"

scalaVersion := "2.9.1"

resolvers ++= Seq(
	"snapshots"	at "http://oss.sonatype.org/content/repositories/snapshots",
	"releases"	at "http://oss.sonatype.org/content/repositories/releases",
    "twitter-repo"          at "http://maven.twttr.com"
)

seq( webSettings :_* )

scalacOptions ++= Seq("-deprecation", "-unchecked")

libraryDependencies ++= {
	val liftVersion = "2.4"
    val finagleVersion = "3.0.0"
	Seq(
		"net.liftweb"					%% "lift-webkit"		    % liftVersion			            % "compile",
		"net.databinder"                %% "dispatch-http"          %  "0.8.6",
		"net.liftweb"                   %% "lift-json"              %   "2.3",
		"org.eclipse.jetty"	 		    %  "jetty-webapp"			% "7.5.4.v20111024"	                % "container; test",
		"ch.qos.logback"		 		%  "logback-classic"		% "1.0.6",
		"org.scalatest"				    %% "scalatest"				% "2.0.M3"				            % "test",
        "com.foursquare"        %% "rogue"                  % "1.1.8",
        "com.twitter"                   %  "scrooge-runtime"            % "1.0.3",
        "com.twitter"                   %% "finagle-core"                       % finagleVersion,
        "com.twitter"                   %% "finagle-thrift"             % finagleVersion
  	)
}

port in container.Configuration := 8081